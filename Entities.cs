using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace GoGame
{
    public interface IStone
    {
        char col { get; }
        uint row { get; }
        Color color { get; }
    }

    // A GoStone has a color and a location (point)
    public sealed class GoStone: IStone
    {
        private readonly IGoPoint _point;
        private readonly Color _color;

        public GoStone(char col, uint row, Color color)
        {
            if (color != Color.Black && color != Color.White)
                throw new ArgumentException("invalid color");

            this._point = new GoPoint(col, row);
            this._color = color;
        }

        public char col { get { return _point.col; } }
        public uint row { get { return _point.row; } }
        public Color color { get { return _color; } }

        public override String ToString()
        {
            return "row:" + row + " col:" + col + " " + color;
        }
    }

    public interface IGoPoint
    {
        // (a,1) is top-left, (t,19) is bottom-right
        char col { get; }
        uint row { get; }
        IList<IGoPoint> neighbors { get; }
    }

    public sealed class GoPoint: IGoPoint
    {
        const uint FIRST_ROW = 1;
        const uint LAST_ROW = 19;
        const char FIRST_COL = 'a';
        const char LAST_COL = 't';

        // using uint (cf int) helps constrain valid input
        private readonly uint _row;

        private readonly char _col;
        public char col { get { return _col; } }
        public uint row { get { return _row; } }

        public GoPoint(char col, uint row)
        {
            if (col < FIRST_COL || col > LAST_COL || row < FIRST_ROW || row > LAST_ROW)
                throw new ArgumentException("out of bounds");

            this._col = col;
            this._row = row;
        }

        public IList<IGoPoint> neighbors
       {
            get {
                IList<IGoPoint> result = new List<IGoPoint>();
                if (HasNorth)
                    result.Add(new GoPoint(col, row - 1));
                if (HasWest)
                    result.Add(new GoPoint((char)(col - 1), row));
                if (HasSouth)
                    result.Add(new GoPoint(col, row + 1));
                if (HasEast)
                    result.Add(new GoPoint((char)(col + 1), row));

                // "assert the post-condition": we *know* this has to be true
                Debug.Assert(result.Count >= 2);
                return result;
            }
        }

        public bool HasNorth
        {
            get { return row > FIRST_ROW; }
        }

        public bool HasSouth
        {
            get { return row < LAST_ROW; }
        }

        public bool HasWest
        {
            get { return col > (int)FIRST_COL; }
        }

        public bool HasEast
        {
            get { return col < (int)LAST_COL; }
        }

        public static bool operator ==(GoPoint p1, GoPoint p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(GoPoint p1, GoPoint p2)
        {
            return !p1.Equals(p2);
        }

        public override bool Equals(object other)
        {
            if (other != null && typeof(IGoPoint).IsAssignableFrom(other.GetType()))
            {
                IGoPoint p = (IGoPoint)other;
                return (row == p.row && col == p.col);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (int)(row * 31 + ((int)(col) * 17));
        }

        public override String ToString()
        {
            return "row:" + row + " col:" + col;
        }
    }
}
