using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using NUnit.Framework;

namespace GoGame
{
    public sealed partial class GoTest: AssertionHelper
    {
        public GoPoint Point(char col, uint row)
        {
            return new GoPoint(col, row);
        }
    }
}
