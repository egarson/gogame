using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using NUnit.Framework;

namespace GoGame
{
    [TestFixture]
    // Inherit from AssertionHelper so we can use Constraints feature of NUnit 2.4+
    public sealed partial class GoTest: AssertionHelper {

        [Test] public void TestNunitVersion()
        {
            // n.b. fails at compile time if NUnit 2.4 isn't available
            Assert.That("hi", Is.EqualTo("hi"), "You don't have NUnit 2.4+..." );
        }

        [Test] public void TestStoneProperties()
        {
            // sanity check
            IStone s = new GoStone('a', 1, Color.Black);
            Expect(s.col == 'a' && s.row == 1 && s.color == Color.Black);
            Expect(s.ToString(), Is.EqualTo("row:1 col:a Color [Black]"));
        }

        [Test] public void TestStoneInBounds()
        {
            new GoStone('a', 1, Color.White);
            new GoStone('b', 2, Color.Black);
            new GoStone('s', 1, Color.Black);
            new GoStone('s', 19, Color.White);
        }

        [Test] public void TestStoneInvalid()
        {
            // Check: oob row, oob col, and invalid color
            Expect(() => new GoStone('a', 20, Color.Black), Throws.ArgumentException);
            Expect(() => new GoStone('u', 1, Color.Black), Throws.ArgumentException);
            Expect(() => new GoStone('b', 2, Color.Pink), Throws.ArgumentException);
        }

        [Test] public void TestPoint()
        {
            IGoPoint p = Point('a', 1);
            Expect(p.col == 'a' && p.row == 1);
        }

        [Test] public void TestPointEquality()
        {
            Expect(Point('a', 1), Is.EqualTo(Point('a', 1)));
            // Exercise operator overloading
            Expect(Point('a', 1) == Point('a', 1));
            Expect(Point('a', 1) != Point('a', 2));
            Expect(Point('a', 1) != Point('b', 1));
        }

        [Test] public void TestPointHasNorth()
        {
            Expect(Point('a',2).HasNorth);
            Expect(Point('t',2).HasNorth);
            Expect(Point('a',1).HasNorth, Is.False);
            Expect(Point('t',1).HasNorth, Is.False);
        }

        [Test] public void TestPointHasSouth()
        {
            Expect(Point('a',1).HasSouth);
            Expect(Point('t',18).HasSouth);
            Expect(Point('a',19).HasSouth, Is.False);
            Expect(Point('t',19).HasSouth, Is.False);
        }

        [Test] public void TestPointHasWest()
        {
            Expect(Point('b',1).HasWest);
            Expect(Point('t',19).HasWest);
            Expect(Point('a',1).HasWest, Is.False);
            Expect(Point('a',19).HasWest, Is.False);
        }

        [Test] public void TestPointHasEast()
        {
            Expect(Point('a',1).HasEast);
            Expect(Point('a',19).HasEast);
            Expect(Point('t',1).HasEast, Is.False);
            Expect(Point('t',19).HasEast, Is.False);
        }

        [Test] public void TestNeighborsWestEdge()
        {
            IGoPoint p = Point('a', 2);
            IList<IGoPoint> expectedNeighbors = new List<IGoPoint>() {
                Point('a', 1), // North
                Point('a', 3), // South
                Point('b', 2) }; // East
            Expect(p.neighbors, Is.EquivalentTo(expectedNeighbors));
        }

        [Test] public void TestNeighborsEastEdge()
        {
            IGoPoint p = Point('t', 2);
            IList<IGoPoint> expectedNeighbors = new List<IGoPoint>() {
                Point('t', 1), // North
                Point('t', 3), // South
                Point('s', 2) }; // West
            Expect(p.neighbors, Is.EquivalentTo(expectedNeighbors));
        }

        [Test] public void TestNeighborsSouthEdge()
        {
            IGoPoint p = Point('b', 19);
            IList<IGoPoint> expectedNeighbors = new List<IGoPoint>() {
                Point('b', 18), // North
                Point('a', 19), // West
                Point('c', 19) }; // East
            Expect(p.neighbors, Is.EquivalentTo(expectedNeighbors));
        }

        [Test] public void TestStoneNeighborsMiddle()
        {
            IGoPoint p = Point('b', 2);
            IList<IGoPoint> expectedNeighbors = new List<IGoPoint>() {
                Point('b', 1), // North
                Point('b', 3), // South
                Point('a', 2), // West
                Point('c', 2) }; // East
            Expect(p.neighbors, Is.EquivalentTo(expectedNeighbors));
        }

        [Test] public void TestStoneInAtari()
        {

        }
    }
}
